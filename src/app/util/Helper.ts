export class Helper {

    public static formatNumber(num: number): string {
        if (num === null) {
            return '0';
        }
        num = Math.round(num * 100) / 100;
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

}