import { Component, OnInit } from '@angular/core';
import { Company } from '../../models/Company';
import { CompanyService } from '../../services/company.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {

  companies: Company[];

  constructor(private companyService: CompanyService, private router: Router) { }

  ngOnInit(): void {
    this.load();
  }

  load() {
    this.companyService.getCompanies()
      .subscribe(companies => this.companies = companies);
  }

  add(): void {
    this.router.navigate([`/add`]);
  }
}
