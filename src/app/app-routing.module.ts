import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyEditComponent } from './components/company-edit/company-edit.component';
import { MainComponent } from './components/main/main.component';

const routes: Routes = [
  { path: 'edit/:id', component: CompanyEditComponent },
  { path: 'add', component: CompanyEditComponent },
  { path: '', component: MainComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
