import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CompanyTopSectionComponent } from './components/company-top-section/company-top-section.component';
import { CompanyListComponent } from './components/company-list/company-list.component';
import { CompanyItemComponent } from './components/company-item/company-item.component';
import { CompanyEditComponent } from './components/company-edit/company-edit.component';
import { MainComponent } from './components/main/main.component';

@NgModule({
  declarations: [
    AppComponent,
    CompanyTopSectionComponent,
    CompanyListComponent,
    CompanyItemComponent,
    CompanyEditComponent,
    MainComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
