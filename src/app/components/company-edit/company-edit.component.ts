import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Company } from 'src/app/models/Company';
import { CompanyService } from '../../services/company.service';

@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.css']
})
export class CompanyEditComponent implements OnInit {

  @ViewChild('logoFile')
  file;

  errors: string[] = [];

  companyId: number;
  companyName?: string;
  logoUrl?: string;
  employees?: number;
  established?: string;
  revenue?: number;
  netIncome?: number
  securities?: number;
  securityPrice?: number;
  dividends?: number;

  constructor(private router: Router, private route: ActivatedRoute, private companyService: CompanyService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let id: number = params['id'];
      if (id > 0) {
        this.companyService.getCompany(params['id']).subscribe(company => {
          this.companyId = company.id;
          this.companyName = company.name;
          this.logoUrl = company.logo;
          this.employees = company.employees;
          this.established = company.established;
          this.revenue = company.revenue;
          this.netIncome = company.netIncome;
          this.securities = company.securities;
          this.securityPrice = company.securityPrice;
          this.dividends = company.dividends;
        });
      }
    });
  }

  upload(): void {
    if (this.file.nativeElement.files.length > 0) {
      let file = this.file.nativeElement.files[0];
      this.companyService.postFile(file).subscribe(uploadResponse => {
        this.logoUrl = uploadResponse.url;
      });
    }
  }

  save(): void {

    if (this.validate()) {
      let company: Company = {
        id: this.companyId,
        name: this.companyName,
        logo: this.logoUrl,
        employees: this.employees,
        established: this.established,
        revenue: this.revenue,
        netIncome: this.netIncome,
        securities: this.securities,
        securityPrice: this.securityPrice,
        dividends: this.dividends
      }
      this.companyService.saveCompany(company).subscribe(() => {
        this.goBack();
      });
    }
  }

  validate(): boolean {
    this.errors = [];

    if (!this.companyName || this.companyName.length < 3) {
      this.errors.push('Company name not specified or too short (min 3 chars)!');
    }
    if (!this.established || new Date(this.established) > new Date()) {
      this.errors.push('Company established date not specified or not in the past!');
    }

    return this.errors.length == 0;
  }

  goBack(): void {
    this.router.navigate([`/`]);
  }
}
