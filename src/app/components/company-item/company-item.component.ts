import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Company } from '../../models/Company';
import { CompanyService } from '../../services/company.service';
import { Helper } from '../../util/Helper';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-item',
  templateUrl: './company-item.component.html',
  styleUrls: ['./company-item.component.css']
})
export class CompanyItemComponent implements OnInit {

  env = environment;
  helper = Helper;

  @Input()
  company: Company;

  @Output()
  companyDelete = new EventEmitter();

  constructor(private companyService: CompanyService, private router: Router) { }

  ngOnInit(): void {
  }

  edit(): void {
    this.router.navigate([`/edit/${this.company.id}`]);
  }

  delete(): void {
    if (confirm('Are you sure you want to delete this company?')) {
      this.companyService.deleteCompany(this.company.id)
        .subscribe(() => this.companyDelete.emit());
    }
  }

}
