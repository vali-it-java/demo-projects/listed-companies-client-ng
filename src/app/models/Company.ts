export class Company {
    id: number;
    name: string;
    logo?: string;
    established?: string;
    employees?: number;
    revenue?: number;
    netIncome?: number;
    securities?: number;
    securityPrice?: number;
    dividends?: number;
    marketCapitalization?: number;
    dividendYield?: number;
}