export class UploadResponse {
    name: string;
    url: string;
    fileType: string;
    size: number;
}