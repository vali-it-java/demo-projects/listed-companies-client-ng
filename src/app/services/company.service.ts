import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Company } from '../models/Company';
import { UploadResponse } from '../models/UploadResponse';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  env = environment;

  constructor(private http: HttpClient) { }

  getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(`${this.env.apiUrl}/companies`);
  }

  getCompany(id: number): Observable<Company> {
    return this.http.get<Company>(`${this.env.apiUrl}/companies/${id}`);
  }

  saveCompany(company: Company): Observable<void> {
    return this.http.post<void>(`${this.env.apiUrl}/companies`, company);
  }

  postFile(file: File): Observable<UploadResponse> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    return this.http.post<UploadResponse>(`${this.env.apiUrl}/files/upload`, formData);
  }

  deleteCompany(id: number): Observable<void> {
    return this.http.delete<void>(`${this.env.apiUrl}/companies/${id}`);
  }
}
